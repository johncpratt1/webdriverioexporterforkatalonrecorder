function convertKatalonSyntax(command, target, value){
    //2 types of tests need special handing:
    if(/Attribute/.test(command)){
       //the assertAttribute and other *Attribute commands use this format for the target: foo@attribute, for example: link=clickhere@href
       //this will separate the attribute value from the target syntax
       var attributeArr = separateAttributeFromTarget(target);
        target = attributeArr[0];
        var attribute = attributeArr[1];
    }
    if(/Table/.test(command)){
        //the assertTable and other *Table commands attach the X and Y coordinate values to the target (example: foo.1.4)
        //this will separate the X and Y values from the target syntax
        var targetwithTableXYArr = separateTableXYFromTarget(target);
        target = targetwithTableXYArr[0];
        var rowIndex = targetwithTableXYArr[1];
        var columnIndex = targetwithTableXYArr[2];
    }
    
    target = formatTarget(target);
    value = formatValue(value);
    command = command.replace("AndWait","");//there should never be a need to request a wait
    if(/waitFor/.test(command)){
        return '//there should be no need for a "waitFor" command. ';
    }
    //learn more about these "Selenese" commands in the Katalon Recorder here:
    //https://docs.katalon.com/katalon-recorder/docs/selenese-selenium-ide-commands-reference.html
    
    switch(command){
        case "addScript":
            //adds some javascript into a script tag with a specific tag id. Format: addScript(scriptContent, scriptTagId).
            return `await browser.execute("var newScript=document.createElement('SCRIPT');newScript.setAttribute('id','`+value+`');newScript.innerText='`+target+`';document.head.appendChild(newScript);");`;
            
        case "addSelection":
            return 'await (' + target + ').selectByVisibleText("' + value +'");';
        
        case "altKeyDown":
            return "//for the Alt key: await browser.keys(['Alt', 'c']); see https://webdriver.io/docs/api/browser/keys";

        case "answerOnNextPrompt":
            //adds input text for prompt
            return 'await browser.sendAlertText("' + target + '"); //***Move this line after the command that opens the prompt.';
            
        case "assertAlert":
        case "verifyAlert":
            //asserts text in an alert box and closes the alert box
            return "var alertText = ''; try{alertText = await browser.getAlertText();} catch (e){}\n" +
            indentGlb + indentGlb + "await expect(alertText).toEqual('" + target + "'); await browser.dismissAlert();";
            
        case "assertNotAlert":
        case "verifyNotAlert":
            //asserts that the text in an alert box is NOT the specified value and closes the alert.
            return "var alertText = ''; try{alertText = await browser.getAlertText();} catch (e){}\n" +
            indentGlb + indentGlb + "await expect(alertText).not.toEqual('" + target + "'); await browser.dismissAlert();";

        case "assertConfirmation":
        case "assertPrompt":
        case "verifyConfirmation":
        case "verifyPrompt":    
            //asserts text in an confirmation/prompt box
            var chooseConfirm = chooseConfirmOnNextPromptGlb;
            chooseConfirmOnNextPromptGlb = true;//reset global variable
            return "var confirmText = ''; try{confirmText = await browser.getAlertText();} catch (e){}\n" +
            indentGlb + indentGlb + "await expect(confirmText).toEqual('" + target + "'); " +
            (chooseConfirm ? "await browser.acceptAlert();" : "await browser.dismissAlert();");
            
        case "assertNotConfirmation":
        case "assertNotPrompt":
        case "verifyNotConfirmation":
        case "verifyNotPrompt":
            //asserts that the text in an confirmation/prompt box is NOT the specified value.
            var chooseConfirm = chooseConfirmOnNextPromptGlb;
            chooseConfirmOnNextPromptGlb = true;//reset global variable
            return "var confirmText = ''; try{confirmText = await browser.getAlertText();} catch (e){}\n" +
            indentGlb + indentGlb + "await expect(confirmText).not.toEqual('" + target + "'); " +
            (chooseConfirm ? "await browser.acceptAlert();" : "await browser.dismissAlert();");
    
        case "assertAlertPresent":
        case "assertConfirmationPresent":
        case "assertPromptPresent":
        case "verifyAlertPresent":
        case "verifyConfirmationPresent":
        case "verifyPromptPresent":
            //asserts that a prompt/alert is present
            return "var alertPresent = true; try{await browser.acceptAlert();} catch (e){response = false;}\n" +
            indentGlb + indentGlb + "await expect(alertPresent).toEqual(true);";

        case "assertAlertNotPresent":
        case "assertConfirmationNotPresent": 
        case "assertPromptNotPresent":
        case "verifyAlertNotPresent":
        case "verifyConfirmationNotPresent": 
        case "verifyPromptNotPresent":
            //asserts that a prompt/alert is absent
            return "var alertPresent = true; try{await browser.acceptAlert();} catch (e){response = false;}\n" +
            indentGlb + indentGlb + "await expect(alertPresent).toEqual(false);";

        case "assertAttribute":
        case "verifyAttribute":
            //the attribute variable was declared with special handling near the top
            //asserts the value of an attribute
            return "await expect(await (" + target + ").getAttribute('" + attribute + "')).toEqual('" + value + "');";  

        case "assertNotAttribute":
        case "verifyNotAttribute":
            //asserts that the value of an attribute is NOT the specified value
            return "await expect(await (" + target + ").getAttribute('" + attribute + "')).not.toEqual('" + value + "');";  

        case "assertBodyText":
        case "verifyBodyText":
            //asserts that the text of the page matches the value
            return "await expect(await (await $('body')).getText()).toEqual('" + target + "');";

        case "assertNotBodyText":
        case "verifyNotBodyText":
            //asserts that the text of the page does NOT match the value
            return "await expect(await (await $('body')).getText()).not.toEqual('" + target + "');";
             
        case "assertChecked":
        case "verifyChecked":
            //asserts that an element is checked.
            return "await expect( await (" + target + ").isSelected()).toEqual(true);";
            
        case "assertNotChecked":
        case "verifyNotChecked":
            //asserts that an element is checked.
            return "await expect( await (" + target + ").isSelected()).toEqual(false);";

        case "assertCookieByName":
        case "verifyCookieByName":
            //assert the value of a cookie
            return "var cookie = await browser.getCookies(['" + target + "']);\n" +
            indentGlb + indentGlb + "await expect(cookie[0].value).toEqual('" + value + "');";

        case "assertNotCookieByName":
        case "verifyNotCookieByName":
            //assert that the cookie does NOT have the value
            return "var cookie = await browser.getCookies(['" + target + "']);\n" +
            indentGlb + indentGlb + "await expect(cookie[0].value).not.toEqual('" + value + "');";   

        case "assertCookiePresent":
        case "verifyCookiePresent":
            return 'await expect(await browser.getCookies([' + target + ']).length).toEqual(1);';

        case "assertCookieNotPresent":
        case "verifyCookieNotPresent":
            return 'await expect(await browser.getCookies([' + target + ']).length).toEqual(0);';

        case "assertCssCount":
        case "verifyCssCount":
            //asserts the number of nodes that match a specified css selector
            target = target.replace("$","$$$$");//this actually replaces the $ with a double $$
            return 'await expect(await(' + target + ').length).toEqual(' + value + ');';

        case "assertNotCssCount":
        case "verifyNotCssCount":
            //asserts that the number of nodes of a specified css selector does not match the given value
            target = target.replace("$","$$$$");//this replaces $ with a double $$
            return 'await expect(await(' + target + ').length).not.toEqual(' + value + ');';
                
        case "assertEditable":
        case "verifyEditable":
            return 'await expect(true).toEqual(await browser.execute("if(arguments[0].nodeName===\'INPUT\' || arguments[0].nodeName===\'TEXTAREA\'){ return !arguments[0].disabled && !arguments[0].readOnly; }else{ return arguments[0].isContentEditable};", '+target+'));//assertEditable';

        case "assertNotEditable":
        case "verifyNotEditable":
            return 'await expect(false).toEqual(await browser.execute("if(arguments[0].nodeName===\'INPUT\' || arguments[0].nodeName===\'TEXTAREA\'){ return !arguments[0].disabled && !arguments[0].readOnly; }else{ return arguments[0].isContentEditable};", '+target+'));';
            /* uses the following function:
            function isEditable(element){
                if(element.nodeName==="INPUT" || element.nodeName==="TEXTAREA"){
                    return !element.disabled && !element.readOnly;
                }else{
                    return element.isContentEditable;
                }
            }
            resource: https://stackoverflow.com/a/60160327
            */
            
        case "assertElementHeight":
        case "verifyElementHeight":
            //asserts the height of an element in pixels 
            return 'await expect(await (' + target + ').getSize("height")).toEqual(' + value + ');'; 
           
        case "assertNotElementHeight":
        case "verifyNotElementHeight":
            //asserts that the height of an element in pixels is NOT a value
            return 'await expect(await (' + target + ').getSize("height")).not.toEqual(' + value + ');'; 
            
        case "assertElementPresent":
        case "verifyElementPresent":
            //asserts that the element is present 
            return 'await expect(await (' + target + ')).toExist();';  

        case "assertElementNotPresent":
        case "verifyElementNotPresent":
            //asserts that the element is absent
            return 'await expect(await (' + target + ')).not.toExist();';  

        case "assertElementPositionLeft":
        case "verifyElementPositionLeft":
            //asserts the # pixels from the left edge of the frame. 
            return 'await expect(await (' + target + ').getLocation("x")).toEqual(' + value + ');'; 
                  
        case "assertNotElementPositionLeft":
        case "verifyNotElementPositionLeft":
            //asserts that the # of pixels for the left edge of the frame does NOT equal the specified value. 
            return 'await expect(await (' + target + ').getLocation("x")).not.toEqual(' + value + ');'; 
        
        case "assertElementPositionTop":
        case "verifyElementPositionTop":
            //asserts the # pixels from the top edge of the frame. 
            return 'await expect(await (' + target + ').getLocation("y")).toEqual(' + value + ');'; 
                    
        case "assertNotElementPositionTop":
        case "verifyNotElementPositionTop":
            //asserts that the # of pixels for the top edge of the frame does NOT equal the specified value. 
            return 'await expect(await (' + target + ').getLocation("y")).not.toEqual(' + value + ');'; 

        case "assertElementWidth":
        case "verifyElementWidth":
            //asserts the width of an element in pixels 
            return 'await expect(await (' + target + ').getSize("width")).toEqual(' + value + ');'; 

        case "assertNotElementWidth":
        case "verifyNotElementWidth":
            //asserts that the width of an element in pixels is not the specified value. 
            return 'await expect(await (' + target + ').getSize("width")).not.toEqual(' + value + ');'; 

        case "assertHtmlSource":
        case "verifyHtmlSource":
            //asserts the entire HTML source 
            return 'await expect(await browser.getPageSource()).toEqual("' + target + '");';

        case "assertNotHtmlSource":
        case "verifyNotHtmlSource":
            //asserts that the entire HTML source is not the specified value
            return 'await expect(await browser.getPageSource()).not.toEqual("' + target + '");';
            
        case "assertLocation":
        case "verifyLocation":
            //asserts the page's URL 
            return 'await expect(await browser.getUrl()).toEqual("' + target + '");';

        case "assertNotLocation":
        case "verifyNotLocation":
            //asserts that the page's URL is not the specified value.
            return 'await expect(await browser.getUrl()).not.toEqual("' + target + '");';

        case "assertSelectOptions":
        case "verifySelectOptions":
            //asserts the value of an array converted to a string of all options (the visible text) in a select tool
            return 'await expect(await browser.execute("return Array.from(arguments[0]).map(option => option.text).join();",'+target+')).toEqual("'+value+'");';

        case "assertNotSelectOptions":
        case "verifyNotSelectOptions":
            return 'await expect(await browser.execute("return Array.from(arguments[0]).map(option => option.text).join();",'+target+')).not.toEqual("'+value+'");';
            
        case "assertSelectedId":
        case "assertSelectedIds":
        case "verifySelectedId":
        case "verifySelectedIds":
            //asserts the value of an array converted to a string of all optionids selected in a select tool
            return 'await expect(await browser.execute("return Array.from(arguments[0]).map(option => option.id).join();",'+target+'.getSelectedOptions())).toEqual("'+value+'");';
        
        case "assertNotSelectedId":
        case "assertNotSelectedIds":
        case "verifyNotSelectedId":
        case "verifyNotSelectedIds":
            return 'await expect(await browser.execute("return Array.from(arguments[0]).map(option => option.id).join();",'+target+'.getSelectedOptions())).not.toEqual("'+value+'");';

        case "assertSelectedIndex":
        case "assertSelectedIndexes":
        case "verifySelectedIndex":
        case "verifySelectedIndexes":
            //asserts the selected index(es). Expecting a string such as "1,2,3" for multiple values.
            return 'await expect(await browser.execute("var indexes = []; Array.from(arguments[0]).forEach(function (option,index){if(option.selected==true)indexes.push(index)}); return indexes.join();",'+target+')).toEqual("'+value+'");//getSelectedIndex';

        case "assertNotSelectedIndex":
        case "assertNotSelectedIndexes":
        case "verifyNotSelectedIndex":
        case "verifyNotSelectedIndexes":
            //asserts the selected index(es) are not the specified value. Expecting a string such as "1,2,3" for multiple values.
            return 'await expect(await browser.execute("var indexes = []; Array.from(arguments[0]).forEach(function (option,index){if(option.selected==true)indexes.push(index)}); return indexes.join();",'+target+')).not.toEqual("'+value+'");//getSelectedIndex';
                        
        case "assertSelectedLabel":
        case "verifySelectedLabel":
            //check the visible text of a selection in a select tool
            return "await expect(await browser.execute('return arguments[0].options[arguments[0].selectedIndex].text', " + target + ")).toEqual('" + value + "');";
            //source: https://stackoverflow.com/a/14976565/1691651
        
        case "assertNotSelectedLabel":
        case "verifyNotSelectedLabel":
            //check the visible text of a selection in a select tool
            return "await expect(await browser.execute('return arguments[0].options[arguments[0].selectedIndex].text', " + target + ")).not.toEqual('" + value + "');";
                        
        case "assertSelectedValue":
        case "verifySelectedValue":
            //asserts the value attribute associated with the selection in a toggle or select element
            return 'await expect(await(' + target + ').getValue()).toEqual("' + value + '");';         
        
        case "assertNotSelectedValue":
        case "verifyNotSelectedValue":
            //asserts that the attribute associated with the selection is NOT the value in a toggle 
            return 'await expect(await(' + target + ').getValue()).not.toEqual("' + value + '");';         
            
        case "assertSomethingSelected":   
        case "verifySomethingSelected":   
            return 'await expect(true).toEqual(await browser.execute("return arguments[0].selectedIndex >= 0", '+target+' )));//assertSomethingSelected';
        
        case "assertNotSomethingSelected": 
        case "verifyNotSomethingSelected":       
            return 'await expect(false).toEqual(await browser.execute("return arguments[0].selectedIndex >= 0", '+target+' ));//assertSomethingSelected';

        case "assertTable":  
        case "verifyTable":  
            //assert the value of a cell. The address of the cell should be in this format in the Katalon Recorder: target.row.column, eg, foo.1.4
            //based on this javascript code where r = row index and c = column index:
            //Note: the rowIndex and columnIndex variables were declared with special handling near the top
            return 'await expect(' + value + ').toEqual(await browser.execute("return arguments[0].children[0].children[arguments[1]].children[arguments[2]].innerText",'+target+', '+rowIndex+','+columnIndex+'));';          
        
        case "assertNotTable":  
        case "verifyNotTable":  
            return 'await expect(' + value + ').not.toEqual(await browser.execute("return arguments[0].children[0].children[arguments[1]].children[arguments[2]].innerText",'+target+', '+rowIndex+','+columnIndex+'));';          

        case "assertText":
        case "verifyText":
            return "await expect(" + target + ").toHaveText('" + value + "');";

        case "assertNotText":
        case "verifyNotText":
            return "await expect(" + target + ").not.toHaveText('" + value + "');";
            
        case "assertTitle":
        case "verifyTitle":
            //assert the page's title tag
            return "await expect(browser).toHaveTitle('" + target + "');";

        case "assertNotTitle":
        case "verifyNotTitle":
            return "await expect(browser).not.toHaveTitle('" + target + "');";
            
        case "assertValue": 
        case "verifyValue":       
            return "await expect(" + target + ").toHaveValue('" + value + "');";

        case "assertNotValue":  
        case "verifyNotValue":      
            return "await expect(" + target + ").not.toHaveValue('" + value + "');";
                        
        case "assertVisible":
        case "verifyVisible":
            return "let isVisible = await browser.execute(function (e) {\n" +
                indentGlb + indentGlb + indentGlb + "return !!( e.offsetWidth || e.offsetHeight || e.getClientRects().length );\n" +
                indentGlb + indentGlb + indentGlb + "//source: https://stackoverflow.com/a/38873788/1691651" +
                indentGlb + indentGlb + "}, " + target + ");\n"+
                indentGlb + indentGlb + "await expect(isVisible).toEqual(true);";

        case "assertNotVisible":
        case "verifyNotVisible":
            return "let isVisible = await browser.execute(function (e) {\n" +
                indentGlb + indentGlb + indentGlb + "return !!( e.offsetWidth || e.offsetHeight || e.getClientRects().length );\n" +
                indentGlb + indentGlb + indentGlb + "//source: https://stackoverflow.com/a/38873788/1691651" +
                indentGlb + indentGlb + "}, " + target + ");\n"+
                indentGlb + indentGlb + "await expect(isVisible).toEqual(false);";
          
        case "assertWhetherThisWindowMatchWindowExpression":
        case "assertWhetherThisFrameMatchFrameExpression":
        case "verifyWhetherThisWindowMatchWindowExpression":
        case "verifyWhetherThisFrameMatchFrameExpression":
            //assert current window/frame name
            return 'await expect(await browser.execute("return window.name")).toEqual("' + target + '");';

        case "assertNotWhetherThisWindowMatchWindowExpression":
        case "assertNotWhetherThisFrameMatchFrameExpression":
        case "verifyNotWhetherThisWindowMatchWindowExpression":
        case "verifyNotWhetherThisFrameMatchFrameExpression":
            return 'await expect(await browser.execute("return window.name")).not.toEqual("' + target + '");';

        case "captureEntirePageScreenshot":
            //different browsers may respond differently. Firefox may take a screenshot of the entire page while Chrome only takes a screenshot of the viewport
            return "await browser.saveScreenshot('" + target + "');";

        case "check":
            //check a checkbox
            return 'await browser.execute("arguments[0].checked = true;", '+target+');';
        case "uncheck":
            //uncheck a checkbox
            return 'await browser.execute("arguments[0].checked = false;", '+target+');';
        
        case "chooseCancelOnNextPrompt" :
        case "chooseCancelOnNextConfirmation" :
            chooseConfirmOnNextPromptGlb = false;
            return '';// the logic for this variable is in the assertConfirmation commands
 
        case "chooseOkOnNextConfirmation" :
            return '';//the default is to confirm, no action is needed

        case "click":
            return "await (" + target + ").click();";
            
        case "clickAt":
            //click an element at specific X and Y coordinates, such as at "10,20"
            var xYArray = value.split(",");
            var xOffset = xYArray[0];
            var yOffset = xYArray[1];
            return "await (" + target + ").click({ x: '" + xOffset + "', y: '" + yOffset + "'});";
            //source: https://webdriver.io/docs/api/element/click

        case "close":
            return "await browser.closeWindow()";

        case "contextMenu":
            //right-click
            return "await (" + target + ").click({ button: 'right' });";
        
        case "controlKeyDown":
            return "//for the control key: await browser.keys(['Control', 'text']);";

        case "shiftKeyDown":
            return "//for the shift key: await browser.keys(['Shift', 'text']);";
            
        case "createCookie":
            var nameValueArray = target.split("=");
            var cookieName = nameValueArray[0];
            var cookieValue = nameValueArray[1];
            return "await browser.setCookies([{name: '" + cookieName + "', value: '" + cookieValue + "'}]);";

            case "deleteAllVisibleCookies":
            return "await browser.deleteCookies();"
            //alternate: browser.deleteAllCookies();
            
        case "deleteCookie":
            return "await browser.deleteCookies(['" + target + "']);";
            //alternate: browser.deleteNamedCookie('name');
                  
        case "doubleClick":
            return "await (" + target + ").doubleClick();";
                    
        case "dragAndDropToObject":  
            value = formatTarget(value);
            return "await (" + target + ").dragAndDrop(" + value + ");";
                                   
        case "echo":
            return `await browser.execute('console.log("*****************for browser console:"); console.log("` + target + `")');`;
            
        case "focus":
            return "await browser.execute('arguments[0].focus()'," + target + ");";
            
        case "goBack":
            return "await browser.back()');";

        case "highlight":
            return `await browser.execute("arguments[0].setAttribute('style','border: solid red; background: yellow')",` + target + `);//highlight`;
       
        case "mouseMoveAt":
            //moves the mouse at an x,y position relative to the element's location
            var xYArray = value.split(",");
            var xOffset = xYArray[0];
            var yOffset = xYArray[1];
            return 'await (' + target + ').moveTo({'+xOffset+','+yOffset+'});';

        case "mouseOver":
            return 'await (' + target + ').moveTo();';
            
        case "mouseOut":
            return '//mouseOut is not supported';
            
        case "open":
            return "await browser.url('" + target + "');";
            
        case "openWindow":
            return "browser.newWindow(" + target + ", {'" + value + "'});";//target is the URL, value is the name

        case "pause":
            return (target)? 'await browser.pause('+target+');' : 'await browser.pause('+value+');'//the pause command can use either a target or a value  

        case "refresh":
            //refresh the current page
            return "await browser.refresh();";
            
        case "removeAllSelections":
            //remove all selections from a multi-select box
            return 'await browser.execute("arguments[0].selectedIndex = -1;",' + target + ');//removeAllSelections';
            //source: https://stackoverflow.com/a/42170881';
            
        case "removeScript":
            //remove a javascript tag and its content
            return 'await browser.execute("arguments[0].remove()",' + target + ');';
        
        case "removeSelection":    
            //unselect an option in a multi-select box. The target should be the multi-select box and the value should be the option text.
            return "//removeSelection is not supported in this plugin";
            
        case "runScript":
            return "await browser.execute('" + target + "');";
            
        case "select":
            //select an option by text in a select tool
            return "await (" + target + ").selectByVisibleText('" + value + "');";

        case "selectFrame":  
            return "await browser.selectFrame("+target+");";//expecting an index number 
            
        case "selectPopUp":
        case "selectWindow":
            return "await browser.switchWindow('" + target + "');";    
            
        case "sendKeys":
        case "type":
        case "typeKeys":
            return `await browser.execute('arguments[0].focus()', ` + target +  `);\n` +
            indentGlb + indentGlb + `await browser.keys('["` + value + `"]');`;
                     
        case "setText":
            //for input fields
            return "await (" + target + ").setValue('" + value + "');";

        case "store":
            //store a value in a variable
            return "var " + target + " = '" + value + "';";
            
        case "submit":
            //click the submit button of a form element
            return `await( await ( await $(' + target + ')).$('[type="submit"]')).click();`
            
        default:
            return "//Unsupported command: " + command + " | target: " + target + " | value: " + value;
    }
    //the URL with a list of these commands is https://docs.katalon.com/katalon-recorder/docs/selenese-selenium-ide-commands-reference.html
}

function formatTarget(target){
    if (!target){
        return '';
    }
    if(/^id=/.test(target)){
        target = target.replace("id=","");
        target = 'await $("#' + target + '")';
    }else if(/^name=/.test(target)){
        target = target.replace("name=","");
        target = `await $('[name="` + target + `"]')`;
    }else if(/^xpath=/.test(target) || /^[/]/.test(target)){
        target = target.replace("xpath=","");
        target = 'await $("' + target + '")';
    }else if(/^link=/.test(target)){
        target = target.replace("link=","");
        target = 'await $("=' + target + '")';
    }else if(/^css=/.test(target)){
        target = target.replace("css=","");
        target = 'await $("' + target + '")';
    }
    return target;
}

function formatValue(value){
    if (!value){
        return '';
    }
    value = value.replace("label=","");
    return value;
}

function separateAttributeFromTarget(target){
    //extract the text after the @ as the attribute name (target@href)
    var targetAttributeArr = /(.*)@(.*)?/.exec(target);//separate the contents after the last @ char.
    target = targetAttributeArr[1];
    var attribute = targetAttributeArr[2];
    return [target,attribute];
}

function separateTableXYFromTarget(target){
    //separate the .y.x after the target for the table coordinates, where y = row index and x = column index (such as tableLocator.row.column) as seen at https://selenium.dev/selenium/docs/api/dotnet/html/M_Selenium_ISelenium_GetTable.htm, ex: foo.1.4
    var targetwithXYcoordinatesArray = /(.*)[.](.*)?[.](.*)?/.exec(target);
    target = targetwithXYcoordinatesArray[1];
    var rowIndex = targetwithXYcoordinatesArray[2];
    var columnIndex = targetwithXYcoordinatesArray[3]; 
    return [target,rowIndex,columnIndex];
}